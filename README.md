# TAU Dissertation LaTeX Template

This repository has been populated starting from v1.2 of the LaTeX
template (published on OverLeaf) made by Ville Koljonen and referenced
at <https://libguides.tuni.fi/dissertationpublishing/template>.

## Original message from the author of the template

> Congratulations, you've made an excellent choice of writing your
> Tampere University dissertation using the LaTeX system.
> This document attempts to be as complete a template as possible to let
> you focus on the most important part: the writing itself.
> Thus the details regarding the visual appearance have already been
> worked out for you!
>
> I sincerely hope you will find this template useful in completing your
> dissertation project. I've tried to add comments (followed by the %
> sign) to clarify the structure and purpose of some of the commands.
> Most of the magic happens in the file taudissertation.cls, which you
> are more than welcome to take a look at. Just refrain from editing it
> in the most crucial versions of the dissertation!
>
> I wish you and your dissertation project the best of luck! If this
> template causes you trouble along the way or if you've any suggestions
> for improving it, please contact me via email at
>
> ville.koljonen (at) tuni.fi.
>
> Yours,
>
> Ville Koljonen
> 5th February 2019

## Notice

> This template or its associated class file don't come with a warranty.
> The content is provided as is, without even the implied promise of
> fitness to the mentioned purpose.
> You, as the author of the dissertation, are responsible for the entire
> work, including the provided material. No one else is liable to you
> for any damage inflicted on you or your thesis, were it caused by
> using this template or not.

## Structure

The template uses `main.tex` as the master LaTeX file for the
dissertation. This file defines the structures and imports the files
with the actual content from `tex/` and the articles from
`publications/`.

Also notice that for the PDF/A requirements, `pdfa-metadata.tex` must be
edited with the relevant metadata.
For more information about the available fields refer to
<https://ctan.org/pkg/pdfx?lang=en>.

## Compiling

A Makefile is included to compile `main.tex`:

~~~
make all
~~~

Will compile it into `_build/main.pdf`, using `latexmk` and run some
automated tests.
In case your LaTeX installation is outdated, or you prefer to run it in
a standardized environment, it is also possible to make any target
defined in the `Makefile` using Docker:

~~~
make -f Makefile.docker all
~~~

## PDF/A Validation

~~~
make validate
~~~

will use `verapdf` inside a Docker cotnainer to run validation of any
pdf file inside `_build/`.
In case it's not passing, you can either try to solve the problem in
LaTeX or use the service at <https://muuntaja.tuni.fi/>.
