SHELL = /bin/bash
TEXMF_ROOT=$(shell kpsewhich --var-value=TEXMFHOME)
DIR_BUILD ?= _build
GARAMOND_ARCHIVE=$(DIR_BUILD)/garamond.tar.gz
ARCHIVE_OUT=$(DIR_BUILD)/taudissertation.tar.gz

SEP = "====="

FILES=taudissertation.cls README.md version-history.txt sRGB_IEC61966-2-1_black_scaled.icc

default: archive

install: $(FILES)
	@set -eu; \
		ddir=$(TEXMF_ROOT)/tex/latex/taudissertation ;\
		mkdir -p $${ddir} ;\
		cp -a $^ $${ddir}/ ;\
		texhash
uninstall:
	@set -eu; \
		ddir=$(TEXMF_ROOT)/tex/latex/taudissertation ;\
		rm -rf $${ddir} ;\
		texhash

$(DIR_BUILD):
	mkdir -p $@
$(GARAMOND_ARCHIVE): | $(DIR_BUILD)
	@echo "$(SEP) Making $@ $(SEP)"
	make ARCHIVE_OUT=$(shell realpath $@) -C garamond archive
$(ARCHIVE_OUT): SAMPLES_FILES=$(shell git ls-tree -r HEAD --name-only -- samples/)
$(ARCHIVE_OUT): $(GARAMOND_ARCHIVE) $(FILES) $(SAMPLES_FILES) Makefile | $(DIR_BUILD)
	@echo "$(SEP) Making $@ $(SEP)"
	git archive \
		--format=tar --prefix=taudissertation/ \
		--add-file=$(GARAMOND_ARCHIVE) \
		HEAD | gzip > $@
	sha256sum $@
	git describe --tags --dirty | tee $@.version
archive: $(ARCHIVE_OUT)


clean:
	@rm -rf $(DIR_BUILD)

distclean: clean
	@make -C samples distclean
	@make -C garamond distclean

.PHONY: default install clean distclean uninstall
