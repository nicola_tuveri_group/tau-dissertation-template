################################################
# Tests
################################################

ALL_TEXSRC=$(MAIN_SOURCES) $(TEXSRC)
ALL_TEXSRC_TESTS=$(patsubst %,test/%,$(ALL_TEXSRC))
ALL_BIBSRC=$(BIBSRC) bib/main_ro.bib
ALL_BIBSRC_TESTS=$(patsubst %,test/%,$(ALL_BIBSRC))
MAIN_LOGS=$(patsubst %,$(DIR_BUILD)/%,$(MAIN_SOURCES:%.tex=%.log))
MAIN_LOGS_TESTS=$(patsubst %,test/%,$(MAIN_LOGS))

test: test-srcs test-logs
test-srcs: $(ALL_TEXSRC_TESTS) $(ALL_BIBSRC_TESTS)
test-logs: $(MAIN_LOGS_TESTS)

$(ALL_TEXSRC_TESTS): test/%.tex: %.tex
	@echo === Testing $<
	@if $(GREP) $(GREP_OPTS) '\\ref{' $<; then \
		echo ""; \
		echo "===== replace ref with cleverref ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -P '\t' $<; then \
		echo ""; \
		echo "===== replace tabs with spaces ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) '[[:blank:]]$$' $<; then \
		echo ""; \
		echo "===== remove trailing whitespace ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) '.\{200\}' $<; then \
		echo ""; \
		echo "===== Long lines detected. Did you hard wrap your text? ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -E '\\(bf|it|tt|rm|sf|sl|em|sc)\>' $<; then \
		echo ""; \
		echo "===== deprecated modal command detected. See https://texfaq.org/FAQ-2letterfontcmd ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -E '\$$\$$' $<; then \
		echo ""; \
		echo '===== do not use $$$$ for math formulae. See 1.6 at https://www.texlive.info/CTAN/info/l2tabu/english/l2tabuen.pdf ====='; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) 'formulae\|formul\\ae' $<; then \
		echo ""; \
		echo "===== we are using formulas for the plural of formula ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -q $$'\r$$' $<; then \
		echo ""; \
		echo "===== Windows line ending detected. Use Linux line endings. ====="; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -P '^([^%]|\\%)*?[^\x00-\x7F]' $<; then \
		echo ""; \
		echo "===== non ASCII character detected: possible typesetting / render issue ====="; \
		exit 1; \
	fi

$(ALL_BIBSRC_TESTS): test/%.bib: %.bib
	@echo === Testing $<
	@if $(GREP) $(GREP_OPTS) -P '^([^%]|\\%)*?[^\x00-\x7F]' $<; then \
		echo ""; \
		echo "===== non ASCII character detected: possible typesetting / render issue ====="; \
		exit 1; \
	fi

$(MAIN_LOGS): %.log: %.pdf
$(MAIN_LOGS_TESTS): test/%.log: %.log
	@echo === Testing $<
	@if grep -q 'There were undefined citations' $<; then \
		echo "fix undefined citations"; \
		exit 1; \
	fi
	@if grep -q 'There were undefined references' $<; then \
		echo "fix undefined references"; \
		exit 1; \
	fi
	@if grep -q 'Warning.*multiply.*defined' $<; then \
		echo "fix duplicate labels"; \
		exit 1; \
	fi
	@if grep -i -q 'No file ' $<; then \
		echo "Missing files in $<"; \
		grep -i 'No file ' $< | sed -e 's/^/> /' ; \
		exit 1; \
	fi

################################################

.PHONY: test test-src test-logs
.PHONY: $(MAIN_LOGS_TESTS) $(ALL_TEXSRC_TESTS) $(ALL_BIBSRC_TESTS)

################################################
