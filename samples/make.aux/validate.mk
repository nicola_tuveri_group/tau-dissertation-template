################################################
# PDF Validation
################################################

DOCKER ?= docker
DOCKER_RUN_OPTS_FOR_VALIDATE=--rm -v /etc/timezone:/etc/timezone:ro -v /etc/localtime:/etc/localtime:ro -v ${PWD}:/data:ro --workdir /data

MAIN_OUTPUTS_VALIDATE=$(patsubst $(DIR_BUILD)/%,validate/%,$(MAIN_OUTPUTS))

DOCKER_IMAGE_FOR_VALIDATE=romen/verapdf:greenfield

validate: $(MAIN_OUTPUTS_VALIDATE)

validate-greenfield: DOCKER_IMAGE_FOR_VALIDATE=romen/verapdf:greenfield
validate-greenfield: validate

validate-pdfbox: DOCKER_IMAGE_FOR_VALIDATE=romen/verapdf:pdfbox
validate-pdfbox: validate

$(MAIN_OUTPUTS_VALIDATE): validate/%.pdf: $(DIR_BUILD)/%.pdf
	$(DOCKER) pull $(DOCKER_IMAGE_FOR_VALIDATE)
	$(DOCKER) run  $(DOCKER_RUN_OPTS_FOR_VALIDATE) $(DOCKER_IMAGE_FOR_VALIDATE) verapdf $<

################################################

.PHONY: validate validate-greenfield validate-pdfbox $(MAIN_OUTPUTS_VALIDATE)

################################################
