#!/bin/env ruby

begin
  require 'nokogiri'
rescue LoadError
  raise "Nokogiri is missing. Try `gem install nokogiri`"
end


doc=Nokogiri::XML(STDIN);

citekeys = doc.xpath("//bcf:section[@number=\"0\"]/bcf:citekey").map {|node| node.text } | [] 

begin
  require 'bibtex' unless ARGV.empty?

  exclude_keys = ARGV.map do |f|
    s = File.readlines(f).reject {|line| /^\s*%/.match? line}.join("")
    b = BibTeX.parse(s)
    b.entries.keys
  end.flatten | []

  citekeys = citekeys - exclude_keys

rescue LoadError
  raise "WARNING: BibTeX-Ruby is missing. Try `gem install bibtex-ruby`"
end

STDERR.puts "citekeys = " + citekeys.inspect
puts citekeys.join("\\|") unless citekeys.empty?
