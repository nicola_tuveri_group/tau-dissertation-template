################################################
# Bibtex handling
################################################

BIBTEX=bibtex
BIBTOOL=bibtool
BIBTOOL_OPT=--preserve.keys=on --preserve.key.case=on --print.align.key=0 --print.line.length=999 --print.use.tab=0 --apply.alias=on -q
BIBALIAS=bib/alias.bib
BIBMASTER_DEFAULT ?= bib/master.bib
BIBAUXDIR=$(DIR_BUILD)/bibaux.dir

ifndef BIBMASTER
ifneq (,$(wildcard $(BIBMASTER_DEFAULT)))
	BIBMASTER=$(BIBMASTER_DEFAULT)
else
	BIBMASTER=bib/master.bib
endif
endif

bib/update: $(MAIN_SOURCES:%.tex=bib/%_ro.bib)
.PHONY: bib/update

bib/force-update: bib/clean bib/update
.PHONY: bib/force-update

$(BIBAUXDIR):
	mkdir -p $(BIBAUXDIR)

$(MAIN_SOURCES:%.tex=bib/%_ro.bib): bib/%_ro.bib: $(BIBAUXDIR)/%.bcf $(BIBSRC) bib/bcf_helper.rb | $(BIBAUXDIR)
	@echo "$(LOG_SEP) Evaluating target $@"
ifneq (,$(wildcard $(BIBMASTER)))
	@echo "INFO: $(BIBMASTER) exists."
	$(eval TMPAUX := $(shell $(MKTEMP) --tmpdir=$(BIBAUXDIR) --suffix=.aux))
	ruby $(lastword $^) $(BIBSRC) < $< > $(TMPAUX)
	@set -eux; \
		if [ -s $(TMPAUX) ]; then \
			$(BIBTOOL) $(BIBTOOL_OPT) -s \
				-X "$$( cat $(TMPAUX))" \
				-i $(BIBMASTER) $(BIBALIAS) > $@ ; \
		else \
			echo "No missing citations to fetch" >&2 ; \
			rm $@; touch $@; \
		fi
	@rm -f $(TMPAUX)
else
	@echo "INFO: $(BIBMASTER) does not exist."
endif

$(MAIN_SOURCES:%.tex=$(BIBAUXDIR)/%.bcf): $(BIBAUXDIR)/%.bcf: %.tex $(TEXSRC) img | $(BIBAUXDIR)
	@echo "$(LOG_SEP) Making $@"
	$(TEXFOT) pdflatex -shell-escape -output-directory $(BIBAUXDIR) $<

bib/clean:
	rm -rf $(BIBAUXDIR)
.PHONY: bib/clean

################################################
