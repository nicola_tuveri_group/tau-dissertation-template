## master bibtex

BibTeX file `main_ro.bib` is meant to be dynamically generated
for those with access to the NISEC `master.bib` file for "golden" entries.
If you do have access, use a symbolic link:

    ln -s /path/to/nisec_tools/manuscripts/bib/master.bib

or use an environmental variable:

    export BIBMASTER=/path/to/nisec_tools/manuscripts/bib/master.bib

Otherwise, put your entries manually in `main_rw.bib`.
This is also the place for bib entries that are not permanent (IACR, arXiv, URLs, ...), or otherwise "unofficial".
If you are unsure, or otherwise not a pedantic editor, put your entry in `main_rw.bib`.
