#!/bin/bash
set -euxo pipefail

IMAGE_NAME=$1
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  )"
cd $SCRIPT_DIR

# Build the container, squash it, and push it to the repository.
buildah bud -f ${DOCKERFILE} -t ${IMAGE_NAME} .
